import re
from pathlib import Path
import pandas as pd
from copy import deepcopy

class WirelistNaiveReader(object):
    def __init__(self,filename):
        self.filename=filename
    def read(self):
        with open(self.filename, 'r') as f:
            raw = f.readlines()
        return self.remainHV_list(raw)

    def remainHV_list(self,lines):
        onlyHV_map={}
        count=0
        for line in lines:
            key='UTaX'
            value='0'
            if re.search('HV',line) != None:
                name=line.split(' ')[1]#space
                pin=lines[count+1].replace(' ','').split('P')[0]
                l = int(len(pin[3:]) / 2)
                key=pin[0:3]+"_"+pin[3:][0:l]
                values=name.split('\n')[0]
                onlyHV_map[key]=values
            count +=1

        return onlyHV_map
    #def HVwire_list(raw_list):

input_dir='HV_PPP_Mapping'
ppp2staves_list = {
    'C-TOP-MAG-TRUE': WirelistNaiveReader(
        input_dir / Path('HVPins_C-Top-Mag-True.NET')).read(),
    'C-BOT-IP-TRUE': WirelistNaiveReader(
        input_dir / Path('HVPins_C-Bot-IP-True.NET')).read(),
    'C-BOT-MAG-MIRROR': WirelistNaiveReader(
        input_dir / Path('HVPins_C-Bot-Mag-Mirror.NET')).read(),
    'C-TOP-IP-MIRROR': WirelistNaiveReader(
        input_dir / Path('HVPins_C-Top-IP-Mirror.NET')).read()
}


output_map={}
for section,data in ppp2staves_list.items():
    for c in ['DS1','DS2','DS3','DS4','DS5','DS6','DS7','DS8']:
      output_map[section + '-' + c] = [[], [], []]
    for key,value in data.items():
        connector=key.split('_')[0]
        pin=key.split('_')[1]
        output_map[section + '-' + connector][0].append(connector)
        output_map[section+'-'+connector][1].append(int(pin))
        output_map[section + '-' + connector][2].append(value)
for key,values in output_map.items():
    if values != []:
      map={}
      map['Connector']=values[0]
      map['PINonPPP']=values[1]
      map['wireNumber']=values[1]
      map['PINonStaves']=values[2]
      df=pd.DataFrame(map)
      df_sorted=df.sort_values(by='PINonPPP')
      df_sorted.index=range(len(values[1]))
      df_sorted.to_csv('./csv/'+key+'.csv')



#print(len(ppp2staves_list['C-TOP-MAG-TRUE']))
#print(ppp2staves_list)
