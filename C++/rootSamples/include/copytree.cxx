#include "TFile.h"
#include "TStyle.h"
#include "TSystem.h"
#include "TMath.h"
#include <cmath>
#include <float.h>
//Making a new tree from an existed tree

void copytree()
{
  //input
  TString oldfilename="buSideband_r16s28r1_Bc2BuKpi_Bu2JpsiK_2016MD_j968.root";//bkg
  //TString oldfilename="DVntuple_j961.root";//signal
  TString newfilename="copytree_"+oldfilename;
  TFile *oldfile=new TFile(oldfilename,"read");
  TTree *tr=(TTree *)oldfile->Get("DecayTree");
  if( tr == NULL )  tr = (TTree*)oldfile->Get("tuple/DecayTree");



  Double_t        Bu_ENDVERTEX_CHI2;
  Int_t           Bu_ENDVERTEX_NDOF;
  Double_t        Bu_DIRA_ORIVX;
  Double_t        Bu_FDCHI2_OWNPV;
  Double_t        Bu_PT;
  Double_t        Jpsi_IPCHI2_OWNPV;
  Double_t        Jpsi_PT;
  Double_t        Kp_IPCHI2_OWNPV;
  Double_t        Kp_PT;
  //transform
  Double_t       log10_1mBu_DIRA_ORIVX;
  Double_t       log10_Bu_FDCHI2_OWNPV;
  Double_t       log10_Kp_IPCHI2_OWNPV;
  Double_t       log10_Jpsi_IPCHI2_OWNPV;
  Double_t       Bu_ENDVERTEX_CHI2NDF;
  //set the BranchStatus
  TString Branches[9]={"Bu_ENDVERTEX_CHI2","Bu_ENDVERTEX_NDOF","Bu_DIRA_ORIVX"
                   ,"Bu_FDCHI2_OWNPV","Bu_PT","Jpsi_IPCHI2_OWNPV","Jpsi_PT","Kp_IPCHI2_OWNPV","Kp_PT"};
  tr->SetBranchStatus("*",0);// silent all
  for (Int_t i = 0; i < 9; i++) {
    /* code */
    TString brancename=Branches[i];
    tr->SetBranchStatus(brancename,1);//active the branches you want
  }
  tr->SetBranchAddress("Bu_ENDVERTEX_CHI2",&Bu_ENDVERTEX_CHI2);
  tr->SetBranchAddress("Bu_ENDVERTEX_NDOF",&Bu_ENDVERTEX_NDOF);
  tr->SetBranchAddress("Bu_DIRA_ORIVX",&Bu_DIRA_ORIVX);
  tr->SetBranchAddress("Bu_FDCHI2_OWNPV",&Bu_FDCHI2_OWNPV);
  tr->SetBranchAddress("Bu_PT",&Bu_PT);

  tr->SetBranchAddress("Jpsi_IPCHI2_OWNPV",&Jpsi_IPCHI2_OWNPV);
  tr->SetBranchAddress("Jpsi_PT",&Jpsi_PT);
  tr->SetBranchAddress("Kp_IPCHI2_OWNPV",&Kp_IPCHI2_OWNPV);
  tr->SetBranchAddress("Kp_PT",&Kp_PT);



  //create new branches
  TFile *newfile= new TFile(newfilename,"recreate");
  TTree* newtree=tr->CloneTree(0);//zero:just copy branches' structure,fill the tree with new Branches later
  newtree->Branch("Bu_ENDVERTEX_CHI2NDF", &Bu_ENDVERTEX_CHI2NDF, "Bu_ENDVERTEX_CHI2NDF/D");
  newtree->Branch("log10_1mBu_DIRA_ORIVX", &log10_1mBu_DIRA_ORIVX, "log10_1mBu_DIRA_ORIVX/D");
  newtree->Branch("log10_Bu_FDCHI2_OWNPV", &log10_Bu_FDCHI2_OWNPV, "log10_Bu_FDCHI2_OWNPV/D");
  newtree->Branch("log10_Kp_IPCHI2_OWNPV", &log10_Kp_IPCHI2_OWNPV, "log10_Kp_IPCHI2_OWNPV/D");
  newtree->Branch("log10_Jpsi_IPCHI2_OWNPV", &log10_Jpsi_IPCHI2_OWNPV, "log10_Jpsi_IPCHI2_OWNPV/D");
  for (Int_t i = 0; i < tr->GetEntries(); i++) {
    /* code */
    tr->GetEntry(i);
    log10_1mBu_DIRA_ORIVX = TMath::Log10(1-Bu_DIRA_ORIVX);
    log10_Bu_FDCHI2_OWNPV = TMath::Log(Bu_FDCHI2_OWNPV)/TMath::Log(10.0);
    log10_Kp_IPCHI2_OWNPV = TMath::Log10(Kp_IPCHI2_OWNPV);
    log10_Jpsi_IPCHI2_OWNPV = TMath::Log10(Jpsi_IPCHI2_OWNPV);
    Bu_ENDVERTEX_CHI2NDF=Bu_ENDVERTEX_CHI2/Bu_ENDVERTEX_NDOF;
    if(!std::isinf(log10_Bu_FDCHI2_OWNPV)&&!std::isnan(log10_Bu_FDCHI2_OWNPV))newtree->Fill();
  }


   newtree->Scan("log10_Bu_FDCHI2_OWNPV","log10_Bu_FDCHI2_OWNPV<-1");
  //create newfile
  //newfile->Delete();
  newtree->Write();

  delete  newtree;
  delete  oldfile;
  delete  newfile;
}
