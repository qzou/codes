#include "TH1.h"
#include "TH2.h"
#include "TH3.h"
#include "THn.h"
#include "THnSparse.h"
#include "TStopwatch.h"
#include "TRandom.h"
#include "TCanvas.h"
#include "TFile.h"
#include "TStyle.h"
#include "TSystem.h"

#ifndef SYMBOL
#define  BkgFile "buSideband_r16s28r1_Bc2BuKpi_Bu2JpsiK_2016MD_j968.root"
#define  MCsigFile "DVntuple_j961.root"
#define  newfile  "McSig_VarComp.root"
#endif
void McSig_VarComp()
{
  //TFile *f1 = new TFile("newfile","recreate");
  TFile *f2 = new TFile(BkgFile,"read");
  TTree *tr = (TTree*)f2->Get("DecayTree");

  Double_t        Bu_ENDVERTEX_CHI2;
  Int_t           Bu_ENDVERTEX_NDOF;
  //Double_t        Bu_ORIVX_CHI2;
  //Int_t           Bu_ORIVX_NDOF;
  Double_t        Bu_DIRA_ORIVX;
  Double_t        Bu_FDCHI2_OWNPV;
  Double_t        Bu_PT;
  Double_t        Jpsi_IPCHI2_OWNPV;
  Double_t        Jpsi_PT;
  Double_t        Kp_IPCHI2_OWNPV;
  Double_t        Kp_PT;

  Double_t        Bu_ENDVERTEX_CHI2NDF;
  Double_t        Bu_ORIVX_CHI2NDF;

  tr->SetBranchAddress("Bu_ENDVERTEX_CHI2",&Bu_ENDVERTEX_CHI2);
  tr->SetBranchAddress("Bu_ENDVERTEX_NDOF",&Bu_ENDVERTEX_NDOF);
  //tr->SetBranchAddress("Bu_ORIVX_CHI2",&Bu_ORIVX_CHI2);
  //tr->SetBranchAddress("Bu_ORIVX_NDOF",&Bu_ORIVX_NDOF);
  tr->SetBranchAddress("Bu_DIRA_ORIVX",&Bu_DIRA_ORIVX);
  tr->SetBranchAddress("Bu_FDCHI2_OWNPV",&Bu_FDCHI2_OWNPV);
  tr->SetBranchAddress("Bu_PT",&Bu_PT);

  tr->SetBranchAddress("Jpsi_IPCHI2_OWNPV",&Jpsi_IPCHI2_OWNPV);
  tr->SetBranchAddress("Jpsi_PT",&Jpsi_PT);
  tr->SetBranchAddress("Kp_IPCHI2_OWNPV",&Kp_IPCHI2_OWNPV);
  tr->SetBranchAddress("Kp_PT",&Kp_PT);

  gStyle->SetHistLineWidth(2);
  gStyle->SetOptStat("ne");
  gStyle->SetHistLineColor(12);
  gStyle->SetLineScalePS(1);
  //gStyle->SetOptStat(0);

  //create histograms
  TH1D *h1=new TH1D("h1","Bu_ENDVERTEX_CHI2NDF",60,0,10);
  TH1D *h2=new TH1D("h2","Bu_DIRA_ORIVX",100,-1.5,1.5);
  TH1D *h3=new TH1D("h3","Bu_FDCHI2_OWNPV",100,0,100);
  TH1D *h4=new TH1D("h4","Bu_PT",100,0,20000);
  TH1D *h5=new TH1D("h5","Jpsi_IPCHI2_OWNPV",100,0,1000);
  TH1D *h6=new TH1D("h6","Jpsi_PT",100,0,10000);
  TH1D *h7=new TH1D("h7","Kp_IPCHI2_OWNPV",100,0,1000);
  TH1D *h8=new TH1D("h8","Kp_PT",100,0,10000);

  for (Int_t i = 0; i < (Int_t)tr->GetEntries(); i++) {
    /* code */
    tr->GetEntry(i);
    Bu_ENDVERTEX_CHI2NDF=Bu_ENDVERTEX_CHI2/Bu_ENDVERTEX_NDOF;
    h1->Fill(Bu_ENDVERTEX_CHI2NDF);
    h2->Fill(Bu_DIRA_ORIVX);
    h3->Fill(Bu_FDCHI2_OWNPV);
    h4->Fill(Bu_PT);
    h5->Fill(Jpsi_IPCHI2_OWNPV);
    h6->Fill(Jpsi_PT);
    h7->Fill(Kp_IPCHI2_OWNPV);
    h8->Fill(Kp_PT);
  }

  TFile *f3 = new TFile(MCsigFile,"read");
  TTree *tr2 = (TTree*)f3->Get("tuple/DecayTree");
  tr2->SetBranchAddress("Bu_ENDVERTEX_CHI2",&Bu_ENDVERTEX_CHI2);
  tr2->SetBranchAddress("Bu_ENDVERTEX_NDOF",&Bu_ENDVERTEX_NDOF);
  //tr->SetBranchAddress("Bu_ORIVX_CHI2",&Bu_ORIVX_CHI2);
  //tr->SetBranchAddress("Bu_ORIVX_NDOF",&Bu_ORIVX_NDOF);
  tr2->SetBranchAddress("Bu_DIRA_ORIVX",&Bu_DIRA_ORIVX);
  tr2->SetBranchAddress("Bu_FD_OWNPV",&Bu_FDCHI2_OWNPV);
  tr2->SetBranchAddress("Bu_PT",&Bu_PT);

  tr2->SetBranchAddress("Jpsi_IPCHI2_OWNPV",&Jpsi_IPCHI2_OWNPV);
  tr2->SetBranchAddress("Jpsi_PT",&Jpsi_PT);
  tr2->SetBranchAddress("Kp_IPCHI2_OWNPV",&Kp_IPCHI2_OWNPV);
  tr2->SetBranchAddress("Kp_PT",&Kp_PT);

  //create histograms
  TH1D *d1=new TH1D("h1","Bu_ENDVERTEX_CHI2NDF",100,0,10);
  TH1D *d2=new TH1D("h2","Bu_DIRA_ORIVX",100,-1.5,1.5);
  TH1D *d3=new TH1D("h3","Bu_FDCHI2_OWNPV",100,0,100);
  TH1D *d4=new TH1D("h4","Bu_PT",100,0,20000);
  TH1D *d5=new TH1D("h5","Jpsi_IPCHI2_OWNPV",100,0,1000);
  TH1D *d6=new TH1D("h6","Jpsi_PT",100,0,10000);
  TH1D *d7=new TH1D("h7","Kp_IPCHI2_OWNPV",100,0,1000);
  TH1D *d8=new TH1D("h8","Kp_PT",100,0,10000);
  for (Int_t i = 0; i < 3000; i++) {//(Int_t)tr2->GetEntries()
    /* code */
    tr2->GetEntry(i);
    Bu_ENDVERTEX_CHI2NDF=Bu_ENDVERTEX_CHI2/Bu_ENDVERTEX_NDOF;
    d1->Fill(Bu_ENDVERTEX_CHI2NDF);
    d2->Fill(Bu_DIRA_ORIVX);
    d3->Fill(Bu_FDCHI2_OWNPV);
    d4->Fill(Bu_PT);
    d5->Fill(Jpsi_IPCHI2_OWNPV);
    d6->Fill(Jpsi_PT);
    d7->Fill(Kp_IPCHI2_OWNPV);
    d8->Fill(Kp_PT);
  }

  /*TFile* f1 = new TFile( newfile, "recreate" );
  TTree* newtree;

  Double_t log10_1mBu_DIRA_ORIVX;
  Double_t log10_Bu_FDCHI2_OWNPV;
  Double_t log10_Kp_IPCHI2_OWNPV;
  Double_t log10_Jpsi_IPCHI2_OWNPV;
  log10_1mBu_DIRA_ORIVX = TMath::Log10(1-Bu_DIRA_ORIVX);
  log10_Bu_FDCHI2_OWNPV = TMath::Log(Bu_FDCHI2_OWNPV)/TMath::Log(10.0);
  log10_Kp_IPCHI2_OWNPV = TMath::Log10(Kp_IPCHI2_OWNPV);
  log10_Jpsi_IPCHI2_OWNPV = TMath::Log10(Jpsi_IPCHI2_OWNPV);
  newtree->Branch("Bu_ENDVERTEX_CHI2NDF", &Bu_ENDVERTEX_CHI2NDF, "Bu_ENDVERTEX_CHI2NDF/D");
  newtree->Branch("log10_1mBu_DIRA_ORIVX", &log10_1mBu_DIRA_ORIVX, "log10_1mBu_DIRA_ORIVX/D");
  newtree->Branch("log10_Bu_FDCHI2_OWNPV", &log10_Bu_FDCHI2_OWNPV, "log10_Bu_FDCHI2_OWNPV/D");
  newtree->Branch("log10_Kp_IPCHI2_OWNPV", &log10_Kp_IPCHI2_OWNPV, "log10_Kp_IPCHI2_OWNPV/D");
  newtree->Branch("log10_Jpsi_IPCHI2_OWNPV", &log10_Jpsi_IPCHI2_OWNPV, "log10_Jpsi_IPCHI2_OWNPV/D");
*/
  //Draw the histograms
  TCanvas *c1=new TCanvas("c1","Variables for B_{u} MVA Selection",3200,1600);
  TCanvas *c2=new TCanvas("c2","Variables for B_{u} MVA Selection",3200,1600);
  c1->Divide(2,2);c2->Divide(2,2);
  TH1D *h[8]={h1,h2,h3,h4,h5,h6,h7,h8};
  TH1D *d[8]={d1,d2,d3,d4,d5,d6,d7,d8};
  d[2]->GetYaxis()->SetRangeUser(0,500);
  d[3]->GetYaxis()->SetRangeUser(0,300);
  d[4]->GetYaxis()->SetRangeUser(0,300);
  d[5]->GetYaxis()->SetRangeUser(0,250);
  d[6]->GetYaxis()->SetRangeUser(0,300);
  d[7]->GetYaxis()->SetRangeUser(0,350);


  for (Int_t j = 0; j < 4; j++) {
    c1->cd(j+1);
    d[j]->SetLineColor(2);
    d[j]->Draw();
    h[j]->Draw("same");

  }
  for (Int_t j = 4; j < 8; j++) {
    c2->cd(j-3);
    d[j]->SetLineColor(2);
    d[j]->Draw();
    h[j]->Draw("same");

  }
  c1->SaveAs("Bu_MVA.pdf");
  c2->SaveAs("Bu_daughterMVA.pdf");
}
