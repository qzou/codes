#include <iostrem>
#include <cmath>
#include "TFile.h"
#include "TStyle.h"
#include "TSystem.h"

//This is a general template for root analysis code segments

//Style Setting index
//e.g You can easily use gStyle->SetTitle("")
/*
SetAxisColor
SetBarOffset
SetBarWidth
SetBit
SetCanvasBorderMode
SetCanvasBorderSize
SetCanvasColor
SetCanvasDefH
SetCanvasDefW
SetCanvasDefX
SetCanvasDefY
SetCanvasPreferGL
SetColorModelPS
SetDateX
SetDateY
SetDrawBorder
SetDrawOption
SetDtorOnly
SetEndErrorSize
SetErrorX
SetFillAttributes
SetFillColor
SetFillColorAlpha
SetFillStyle
SetFitFormat
SetFrameBorderMode
SetFrameBorderSize
SetFrameFillColor
SetFrameFillStyle
SetFrameLineColor
SetFrameLineStyle
SetFrameLineWidth
SetFuncColor
SetFuncStyle
SetFuncWidth
SetGridColor
SetGridStyle
SetGridWidth
SetHatchesLineWidth
SetHatchesSpacing
SetHeaderPS
SetHistFillColor
SetHistFillStyle
SetHistLineColor
SetHistLineStyle
SetHistLineWidth
SetHistMinimumZero
SetHistTopMargin
SetImageScaling
SetIsReading
SetJoinLinePS
SetLabelColor
SetLabelFont
SetLabelOffset
SetLabelSize
SetLegendBorderSize
SetLegendFillColor
SetLegendFont
SetLegendTextSize
SetLegoInnerR
SetLineAttributes
SetLineColor
SetLineColorAlpha
SetLineScalePS
SetLineStyle
SetLineStyleString
SetLineWidth
SetMarkerAttributes
SetMarkerColor
SetMarkerColorAlpha
SetMarkerSize
SetMarkerStyle
SetName
SetNameTitle
SetNdivisions
SetNumberContours
SetObjectStat
SetOptDate
SetOptFile
SetOptFit
SetOptLogx
SetOptLogy
SetOptLogz
SetOptStat
SetOptTitle
SetPadBorderMode
SetPadBorderSize
SetPadBottomMargin
SetPadColor
SetPadGridX
SetPadGridY
SetPadLeftMargin
SetPadRightMargin
SetPadTickX
SetPadTickY
SetPadTopMargin
SetPaintTextFormat
SetPalette
SetPaperSize
SetScreenFactor
SetStatBorderSize
SetStatColor
SetStatFont
SetStatFontSize
SetStatFormat
SetStatH
SetStatStyle
SetStatTextColor
SetStatW
SetStatX
SetStatY
SetStripDecimals
SetTextAlign
SetTextAngle
SetTextAttributes
SetTextColor
SetTextColorAlpha
SetTextFont
SetTextSize
SetTextSizePixels
SetTickLength
SetTimeOffset
SetTitle
SetTitleAlign
SetTitleBorderSize
SetTitleColor
SetTitleFillColor
SetTitleFont
SetTitleFontSize
SetTitleH
SetTitleOffset
SetTitlePS
SetTitleSize
SetTitleStyle
SetTitleTextColor
SetTitleW
SetTitleX
SetTitleXOffset
SetTitleXSize
SetTitleY
SetTitleYOffset
SetTitleYSize
SetUniqueID
*/
//------------------------------------------------------------------------------

//IO commands
/*
RECREATE	Create a new file, if the file already exists it will be overwritten.
UPDATE	Open an existing file for writing. If no file exists, it is created.
READ	Open an existing file for reading (default).
NET	Used by derived remote file access classes, not a user callable option.
WEB	Used by derived remote http access class, not a user callable option.
*/

TFile f("demo.root","recreate");//or
TFile *f=new TFile("","");
f->GetListOfKeys();
f->Write();
//open a file remotely
TFile *f1 = TFile::Open("local/file.root","update")
TFile *f2 = TFile::Open("root://my.server.org/data/file.root","new")
TFile *f3 = TFile::Open("http://root.cern.ch/files/hsimple.root")
//reach a dir /change a dir
TH1F *h = (TH1F*)gDirectory->Get("myHist"); // or
TH1F *h = (TH1F*)gDirectory->GetList()->FindObject("myHist");
h->SetDirectory(newDir);

//read an object
TTree *tr = (TTree*)f->Get("treePointer");
TTree *tr;f->GetObject("treePointer",tr);
//------------------------------------------------------------------------------
//global variables
gROOT->GetListOf...//These methods return a TSeqCollection, meaning a collection of objects
//eg. gROOT->GetListOfCanvases()->FindObject("c1");
static struct pointer{
  gFile
  gDirectory
  gRandom
  gEnv
  etc.
}

//commonly used object constructor
 TFile/TTree/TBranch/TH1/TGrahp/TF1/TLegend
//use arguments in ROOT scripts
1)eg. void example(TString *name){};
when you excute is ,just input as follow: .x example.C("name")
2)use gROOT->ProcessLine(".x example(\"name\")")

// Setting the Include Path
gSystem->AddIncludePath(" -I$HOME/mypackage/include ");
gSystem->AddLinkedLibs("-L/my/path -lanylib");
gSystem->Load("mydir/mylib");
